﻿using UnityEngine;
using System.Collections;

using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float m_Speed = 5.0f;
    [SerializeField] private float m_JumpVelocity = 5.0f;

    [SerializeField] private ParticleSystem m_DustParticles;

    [Header("Attack")]
    [SerializeField] private GameObject m_MagicAttack;
    [SerializeField] private GameObject m_AttackOrigin;

    [SerializeField] private float m_MagicAtackSpeed = 1f;
    [SerializeField] private float m_MagicAtackCooldown = 1f;
    [SerializeField] private float m_MagicAtackTimeout = 0f;

    private bool m_IsMagicProjectile = true;

    [Header("Ground Checker")]
    [SerializeField] private GameObject m_GroundCheckPoint;
    [SerializeField] private float m_GroundCheckRadius = 1f;
    [SerializeField] private LayerMask m_GroundLayer;

    [Header("Debug")]
    [SerializeField] private bool m_CanDie = true;
    [SerializeField] private Laser m_Laser;
    [SerializeField] private LayerMask[] m_LaserLayers;
    [SerializeField] private SpriteRenderer m_LaserPoint;
    private int m_LaserMask = 0;

    private bool m_IsFacingRight = true;
    private bool m_IsGrounded = false;
    private bool m_IsJumping = false;
    private bool m_WasDoubleJumpUsed = false;
    private bool m_IsDead = false;

    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;

    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();

        for (int i = 0; i < m_LaserLayers.Length; i++)
        {
            m_LaserMask |= m_LaserLayers[i].value;
        }
    }

    void Update ()
    {
        if (m_IsDead || GameManager.Instance.GameOver)
        {
            // reset stuff if game ended
            gameObject.layer = LayerMask.NameToLayer("Death");
            m_Laser.SetPoints(Vector3.zero, Vector3.zero);
            m_LaserPoint.enabled = false;
            m_Animator.SetBool("IsWalking", false);

            return;
        }

        Vector2 mousePos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        if (m_IsMagicProjectile && Input.GetMouseButton(0) && m_MagicAtackTimeout <= 0)
        {
            m_MagicAtackTimeout = m_MagicAtackCooldown;

            // magic attack
            Vector3 projectilePos = m_AttackOrigin.transform.position;
            projectilePos.z = -2; // set this closer to the camera o the trail renderer gets drawn over sprites

            var projectile = Instantiate(m_MagicAttack, projectilePos, Quaternion.identity);

            m_AttackOrigin.transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.1f).OnComplete(
                () => { m_AttackOrigin.transform.localScale = new Vector3(1f, 1f, 1f); }
            );

            Vector2 curPos = new Vector2(m_AttackOrigin.transform.position.x, m_AttackOrigin.transform.position.y);
            mousePos.y += Random.Range(-0.1f, 0.1f);
            var direction = (mousePos - curPos).normalized;
            projectile.GetComponent<Rigidbody2D>().velocity = direction * m_MagicAtackSpeed;
        }

        if (!m_IsMagicProjectile && Input.GetMouseButton(0))
        {
            // laser attack

            Vector3 startPos = m_AttackOrigin.transform.position;
            startPos.z = -1;

            Vector2 curPos = new Vector2(m_AttackOrigin.transform.position.x, m_AttackOrigin.transform.position.y);
            var direction = (mousePos - curPos).normalized;
            var hit = Physics2D.Raycast(new Vector2(m_AttackOrigin.transform.position.x, m_AttackOrigin.transform.position.y),
                                        direction, 100f, m_LaserMask);

            if (hit)
            {
                if (hit.collider.gameObject.tag == "Enemy")
                {
                    hit.collider.gameObject.GetComponent<EnemyHealth>().TakeDamage(0.1f);
                }

                Vector3 endPos = new Vector3(hit.point.x, hit.point.y, -1f);

                m_LaserPoint.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y, -1.5f);
                m_LaserPoint.enabled = true;

                m_Laser.SetPoints(startPos, endPos);
            }
        }

        if (!m_IsMagicProjectile && Input.GetMouseButtonUp(0))
        {
            m_LaserPoint.enabled = false;
            m_Laser.SetPoints(Vector3.zero, Vector3.zero);
        }


        m_MagicAtackTimeout -= Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (m_IsDead || GameManager.Instance.GameOver)
        {
            return;
        }

        float xAxis = Input.GetAxis("Horizontal");
        float xAxisRaw = Input.GetAxisRaw("Horizontal");

        // flip sprite
        if (xAxisRaw > 0 && !m_IsFacingRight)
        {
            m_IsFacingRight = true;
            Flip();
        }
        else if (xAxisRaw < 0 && m_IsFacingRight)
        {
            m_IsFacingRight = false;
            Flip();
        }

        // walking anim
        if (xAxisRaw == 0)
        {
            m_Animator.SetBool("IsWalking", false);
        }
        else
        {
            m_Animator.SetBool("IsWalking", true);

            if (m_IsGrounded)
            {
                m_DustParticles.gameObject.transform.localRotation = Quaternion.Euler(0, 0, 90);
                m_DustParticles.Emit(1);
            }
        }

        var moveSpeed = xAxis * m_Speed;
        m_RigidBody.velocity = new Vector2(moveSpeed, m_RigidBody.velocity.y);

        var other = Physics2D.OverlapCircle(m_GroundCheckPoint.transform.position,
                                            m_GroundCheckRadius, m_GroundLayer.value);

        if (other)
        {
            m_IsGrounded = true;

            // reset jump
            m_IsJumping = false;
            m_WasDoubleJumpUsed = false;
        }
        else
        {
            m_IsGrounded = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (m_IsGrounded || !m_WasDoubleJumpUsed)
            {
                if (m_IsJumping)
                {
                    m_WasDoubleJumpUsed = true;

                    m_DustParticles.gameObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    m_DustParticles.Emit(10);
                }

                StartCoroutine(Jump());
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (!m_CanDie) return; // god mode for debugging

        if (other.gameObject.tag == "Enemy")
        {
            Die();
        }
    }

    void Die()
    {
        m_IsDead = true;

        m_Laser.SetPoints(Vector3.zero, Vector3.zero);
        m_LaserPoint.enabled = false;
        m_Animator.SetTrigger("Die");
        gameObject.layer = LayerMask.NameToLayer("Death");
        GameManager.Instance.SetGameOverScreen();
    }

    IEnumerator Jump()
    {
        m_IsJumping = true;

        m_RigidBody.velocity = Vector2.zero;
        m_RigidBody.AddForce(new Vector2(0f, m_JumpVelocity), ForceMode2D.Impulse);

        while (Input.GetKey(KeyCode.Space) && m_RigidBody.velocity.y > 0f)
        {
            yield return null;
        }

        if (m_RigidBody.velocity.y > 0f)
        {
            m_RigidBody.velocity = new Vector2(m_RigidBody.velocity.x, 0f);
        }
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(m_GroundCheckPoint.transform.position, m_GroundCheckRadius);
    }

    public void SetMagicAttack(GameObject magic, float attackCooldown, bool isProjectile)
    {
        m_MagicAttack = magic;

        m_MagicAtackCooldown = attackCooldown;
        m_IsMagicProjectile = isProjectile;
        m_Laser.SetPoints(Vector3.zero, Vector3.zero);
        m_LaserPoint.enabled = false;
    }
}
