﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Animator))]
public class MagicBounceAttack : MonoBehaviour
{
    [SerializeField] private float m_Damage = 1f;
    [SerializeField] private float m_Speed = 3f;
    [SerializeField] private int m_MaxHopCount = 3;

    private Vector2 m_Direction;
    private int m_HopCount = 0;

    private Rigidbody2D m_RigidBody;
    private CircleCollider2D m_Collider;
    private Animator m_Animator;

    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Collider = GetComponent<CircleCollider2D>();
        m_Animator = GetComponent<Animator>();

        m_Direction = m_RigidBody.velocity.normalized;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            var health = other.gameObject.GetComponent<EnemyHealth>();

            if (health)
            {
                health.TakeDamage(m_Damage);
            }

            StartCoroutine(Explode());
        }
        else
        {
            m_HopCount++;

            Vector2 newDirection = Vector2.Reflect(m_Direction, other.contacts[0].normal);

            m_RigidBody.velocity = newDirection.normalized * m_Speed;
            m_Direction = newDirection.normalized;
        }

        if (m_HopCount >= m_MaxHopCount)
        {
            StartCoroutine(Explode());
        }
    }

    IEnumerator Explode()
    {
        m_Collider.enabled = false;
        m_RigidBody.velocity = Vector2.zero;
        m_Animator.SetTrigger("Destroy");

        yield return new WaitForSeconds(0.25f);

        Destroy(this.gameObject);
    }
}
