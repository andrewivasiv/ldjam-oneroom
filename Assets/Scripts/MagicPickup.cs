﻿using UnityEngine;

public class MagicPickup : MonoBehaviour
{
    [SerializeField] private Sprite m_OrbSprite;
    [SerializeField] private GameObject m_Projectile;

    [SerializeField] private float m_Cooldown;
    [SerializeField] private bool m_IsProjectile;

    public Sprite OrbSprite { get { return m_OrbSprite; } }
    public GameObject Projectile { get { return m_Projectile; } }

    public float Cooldown { get { return m_Cooldown; } }
    public bool IsProjectile { get { return m_IsProjectile; } }

    public void OnPickUp()
    {
        GameManager.Instance.ResetPickup();
        GameManager.Instance.UpdateScore(10);
    }
}
