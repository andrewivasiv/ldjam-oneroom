﻿using UnityEngine;

public class SmoothMovement : MonoBehaviour
{
    [SerializeField] private float m_Speed = 1f;
    [SerializeField] private GameObject m_Target;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, m_Target.transform.position, m_Speed * Time.deltaTime);
    }
}
