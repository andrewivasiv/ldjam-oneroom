﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour
{
    private LineRenderer m_Laser;

    [SerializeField][RangeAttribute(-1,0)] private float m_MinXOffset = 0f;
    [SerializeField][RangeAttribute(0, 1)] private float m_MaxXOffset = 0f;
    [SerializeField][RangeAttribute(-1,0)] private float m_MinYOffset = 0f;
    [SerializeField][RangeAttribute(0, 1)] private float m_MaxYOffset = 0f;

    void Start()
    {
        m_Laser = GetComponent<LineRenderer>();
    }

    public void SetPoints(Vector3 startPoint, Vector3 endPoint)
    {
        if (startPoint == Vector3.zero && endPoint == Vector3.zero)
        {
            for (int i = 0; i < 8; i++)
            {
                m_Laser.SetPosition(i+1, Vector3.zero);
            }

            return;
        }

        for (int i = 1; i < 9; i++)
        {
            float k = i * 0.1f;
            float x = startPoint.x + k * (endPoint.x - startPoint.x) + Random.Range(m_MinXOffset, m_MaxXOffset);
            float y = startPoint.y + k * (endPoint.y - startPoint.y) + Random.Range(m_MinYOffset, m_MaxYOffset);

            m_Laser.SetPosition(i, new Vector3(x, y, -1));
        }

        m_Laser.SetPosition(0, startPoint);
        m_Laser.SetPosition(9, endPoint);
    }
}
