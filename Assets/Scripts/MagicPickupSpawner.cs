﻿using UnityEngine;

public class MagicPickupSpawner : MonoBehaviour
{
    [SerializeField] private Transform[] m_SpawnLocations;
    [SerializeField] private GameObject[] m_Pickups;

    [SerializeField] private LayerMask m_Mask;
    [SerializeField] private float m_Radius = 1f;

    public void SpawnPickUp()
    {
        int locationIndex = Random.Range(0, m_SpawnLocations.Length);
        int pickupIndex = Random.Range(0, m_Pickups.Length);

        var other = Physics2D.OverlapCircle(m_SpawnLocations[locationIndex].position,
                                            m_Radius, m_Mask.value);

        while (other != null)
        {
            locationIndex = Random.Range(0, m_SpawnLocations.Length);

            other = Physics2D.OverlapCircle(m_SpawnLocations[locationIndex].position,
                                            m_Radius, m_Mask.value);
        }

        Instantiate(m_Pickups[pickupIndex], m_SpawnLocations[locationIndex].position, Quaternion.identity);
    }

    void OnDrawGizmosSelected()
    {
        foreach (var transform in m_SpawnLocations)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, m_Radius);
        }
    }
}
