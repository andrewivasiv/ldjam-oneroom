﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Animator))]
public class MagicAttack : MonoBehaviour
{
    [SerializeField] private float m_Damage = 1f;

    private Rigidbody2D m_RigidBody;
    private CircleCollider2D m_Collider;
    private Animator m_Animator;

    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Collider = GetComponent<CircleCollider2D>();
        m_Animator = GetComponent<Animator>();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            var health = other.gameObject.GetComponent<EnemyHealth>();

            if (health)
            {
                health.TakeDamage(m_Damage);
            }
        }

        StartCoroutine(Explode());
    }

    IEnumerator Explode()
    {
        m_Collider.enabled = false;
        m_RigidBody.velocity = Vector2.zero;
        m_Animator.SetTrigger("Destroy");

        yield return new WaitForSeconds(0.25f);

        Destroy(this.gameObject);
    }
}
