﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private Text m_ScoreText;
    [SerializeField] private Text m_GameOverText;

    [Header("Data")]

    [SerializeField] private Spawner m_Spawner;
    [SerializeField] private MagicPickupSpawner m_PickupSpawner;

    [Header("Game State")]
    private bool m_IsGameOver = false;
    private bool m_IsPickupPresent = false;

    private int m_Score = 0;

    private static GameManager m_Instance = null;

    void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    void Start()
    {
        m_PickupSpawner = GetComponent<MagicPickupSpawner>();

        StartCoroutine(SpawnPickup());
    }

    IEnumerator SpawnPickup()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1f, 3f));

            if (!m_IsPickupPresent)
            {
                m_PickupSpawner.SpawnPickUp();
                m_IsPickupPresent = true;
            }
        }
    }

    public bool GameOver { get { return m_IsGameOver; } }
    public static GameManager Instance { get { return m_Instance; }}

    public void UpdateScore(int scorePoints)
    {
        m_Score += scorePoints;

        m_ScoreText.text = "Score: " + m_Score;
    }

    public void SetGameOverScreen()
    {
        m_IsGameOver = true;
        m_Spawner.gameObject.SetActive(false);
        m_GameOverText.gameObject.SetActive(true);
    }

    public void ResetPickup()
    {
        m_IsPickupPresent = false;
    }
}
