﻿using UnityEngine;

[RequireComponent(typeof(EnemyBehaviour))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float m_Health = 1f;

    private EnemyBehaviour m_Behaviour;

    void Start()
    {
        m_Behaviour = GetComponent<EnemyBehaviour>();
    }

    public void TakeDamage(float damage)
    {
        m_Health -= damage;

        if (m_Health <= 0)
        {
            m_Behaviour.Death();
        }
    }
}
