﻿using UnityEngine;

public class WeaponSelector : MonoBehaviour
{
    [SerializeField] private PlayerController m_Controller;
    [SerializeField] private SpriteRenderer m_OrbRenderer;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Pickup")
        {
            var pickup = other.gameObject.GetComponent<MagicPickup>();

            m_OrbRenderer.sprite = pickup.OrbSprite;
            m_Controller.SetMagicAttack(pickup.Projectile, pickup.Cooldown, pickup.IsProjectile);

            pickup.OnPickUp();

            Destroy(other.gameObject);
        }
    }

}
