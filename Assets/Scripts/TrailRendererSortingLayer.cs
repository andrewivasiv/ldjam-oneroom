﻿using UnityEngine;

[RequireComponent(typeof(TrailRenderer))]
public class TrailRendererSortingLayer : MonoBehaviour
{
    [SerializeField] private string  m_SortLayer = "Default";
    [SerializeField] private int m_SortOrder = 0;

    void Start()
    {
        var renderer = GetComponent<TrailRenderer>();

        renderer.sortingLayerName = m_SortLayer;
        renderer.sortingOrder = m_SortOrder;
    }
}
