﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuCommands : MonoBehaviour
{
    [SerializeField] private Button m_PlayButton;
    [SerializeField] private Button m_TutorialButton;

    [SerializeField] private Button m_BackButton;
    [SerializeField] private GameObject m_Tutorial;

    public void OnTutorialButton()
    {
        m_PlayButton.gameObject.SetActive(false);
        m_TutorialButton.gameObject.SetActive(false);

        m_BackButton.gameObject.SetActive(true);
        m_Tutorial.SetActive(true);
    }

    public void OnBackButton()
    {
        m_PlayButton.gameObject.SetActive(true);
        m_TutorialButton.gameObject.SetActive(true);

        m_BackButton.gameObject.SetActive(false);
        m_Tutorial.SetActive(false);
    }
}
