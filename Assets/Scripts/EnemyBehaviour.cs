﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField] private float m_Speed = 1.0f;
    [SerializeField] private GameObject m_Target;
    [SerializeField] private LayerMask m_PathFindingMask;
    [SerializeField] private bool m_IsFacingRight = false;
    [SerializeField] private int m_PointValue = 1;

    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;

    void Start()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        var direction = m_IsFacingRight ? Vector2.right : Vector2.left;
        var hit = Physics2D.Raycast(transform.position, direction, 0.3f, m_PathFindingMask.value);

        if (hit)
        {
            m_IsFacingRight = !m_IsFacingRight;
            m_Speed *= -1;

            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }

        m_RigidBody.velocity = new Vector2(m_Speed, m_RigidBody.velocity.y);
    }

    public void SetTarget(GameObject target)
    {
        m_Target = target;
    }

    public void SetDirectionAndSpeed(bool right, float speed)
    {
        m_IsFacingRight = right;
        m_Speed = speed;
    }

    public void Death()
    {
        StartCoroutine(DeathAnimation());
    }

    IEnumerator DeathAnimation()
    {
        m_Speed = 0;
        gameObject.layer = LayerMask.NameToLayer("Death");
        m_Animator.SetTrigger("Destroy");

        GameManager.Instance.UpdateScore(m_PointValue);

        yield return new WaitForSeconds(0.5f);

        Destroy(this.gameObject);
    }
}
