﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Enemies;
    [SerializeField] private float m_MinTime = 1f;
    [SerializeField] private float m_MaxTime = 3f;

    private float m_RageTimer = 0f;
    private float m_RageIncreaseTime = 10f;
    private int m_RageIncreases = 0;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    void Update()
    {
        m_RageTimer += Time.deltaTime;

        if (m_RageTimer >= m_RageIncreaseTime)
        {
            m_RageIncreases++;
            m_RageTimer = 0;

            m_MinTime -= 0.2f;
            m_MaxTime -= 0.4f;

            if (m_RageIncreases == 3)
            {
                m_RageIncreaseTime += 10000f;
            }
            else
            {
                m_RageIncreaseTime += 10f;
            }
        }
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(1f);

        // TODO check game state
        while (true)
        {
            var enemyIndex = Random.Range(0, m_Enemies.Length);
            Instantiate(m_Enemies[enemyIndex], transform.position, Quaternion.identity);

            yield return new WaitForSeconds(Random.Range(m_MinTime, m_MaxTime));
        }
    }
}
