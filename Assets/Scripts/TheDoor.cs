﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class TheDoor : MonoBehaviour
{
    private Animator m_Animator;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            m_Animator.SetTrigger("Open");
            GameManager.Instance.SetGameOverScreen();
        }
    }
}
